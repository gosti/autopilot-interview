package models

import "encoding/json"

type Fields map[string]interface{}

type Contact struct {
	ID     string `json:"id"`
	Fields Fields `json:"fields"`
}

func (c Contact) MarshalBinary() ([]byte, error) {
	return json.Marshal(c)
}

func (c *Contact) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, &c)
}
