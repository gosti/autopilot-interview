//go:generate mockgen -destination=../../mocks/mock_contact.go -package=mocks autopilot-interview/repositories/contact ContactProvider
package contact

import (
	"autopilot-interview/models"
	"context"
)

type ConctactUpdateResponse struct {
	Contact *models.Contact

	Status string
}

type ContactProvider interface {
	Get(ctx context.Context, ID string) (*models.Contact, error)
	Update(ctx context.Context, contact *models.Contact) (*ConctactUpdateResponse, error)

	Status(ctx context.Context) error
}
