package contact

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	ErrNotFound   ContactError = ContactError{message: "not found", Code: http.StatusNotFound}
	ErrBadRequest ContactError = ContactError{message: "bad request", Code: http.StatusBadRequest}
	ErrUnexpected ContactError = ContactError{message: "got an unexpected error"}
)

type ContactError struct {
	message string
	value   string
	Err     error
	Code    int
}

func (err ContactError) Unwrap() error {
	return err.Err // Returns inner error
}

func (e ContactError) Error() string {
	return fmt.Sprintf("Contact: %s %s", e.value, e.message)
}

func (e ContactError) Is(target error) bool {
	var targetContactError *ContactError
	return errors.As(target, &targetContactError) &&
		e.message == targetContactError.message &&
		e.Code == targetContactError.Code
}

func NewErrNotFound(ID string, err error) *ContactError {
	newErr := ErrNotFound
	newErr.value = ID
	newErr.Err = err

	return &newErr
}

func NewErrBadRequest(ID string, err error) *ContactError {
	newErr := ErrBadRequest
	newErr.value = ID
	newErr.Err = err

	return &newErr
}

func NewErrUnexpected(ID string, code int, err error) *ContactError {
	return &ContactError{message: ErrUnexpected.message, value: ID, Code: code, Err: err}
}
