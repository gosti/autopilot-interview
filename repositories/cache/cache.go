//go:generate mockgen -destination=../../mocks/mock_cache.go -package=mocks autopilot-interview/repositories/cache CacheProvider
package cache

import (
	"autopilot-interview/models"
	"context"
	"time"
)

type CacheResponse struct {
	Contact *models.Contact

	TTL time.Duration
}

type CacheProvider interface {
	Get(ctx context.Context, ID string) (*CacheResponse, error)
	Update(ctx context.Context, contact *models.Contact) (*CacheResponse, error)

	Invalidate(ctx context.Context) error

	Status(context.Context) error
}
