package cache

import (
	"errors"
	"fmt"
)

var (
	ErrNotFound CacheError = CacheError{message: "not found"}
	ErrExpired  CacheError = CacheError{message: "is expired"}
)

type CacheError struct {
	message string
	value   string
	Err     error
}

func (err CacheError) Unwrap() error {
	return err.Err // Returns inner error
}

func (e CacheError) Error() string {
	return fmt.Sprintf("Cache: %s %s", e.value, e.message)
}

func (e CacheError) Is(target error) bool {
	var targetCacheError *CacheError
	return errors.As(target, &targetCacheError) && e.message == targetCacheError.message
}

func NewErrNotFound(ID string, err error) *CacheError {
	return &CacheError{message: ErrNotFound.message, value: ID, Err: err}
}

func NewErrExpired(ID string, err error) *CacheError {
	return &CacheError{message: ErrExpired.message, value: ID, Err: err}
}
