FROM golang:1.17-alpine

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait


WORKDIR /api


COPY go.* ./
RUN go mod download

COPY . ./

RUN go build ./cmd/api

EXPOSE 8080

CMD /wait && ./api