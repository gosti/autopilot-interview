// Code generated by MockGen. DO NOT EDIT.
// Source: autopilot-interview/repositories/cache (interfaces: CacheProvider)

// Package mocks is a generated GoMock package.
package mocks

import (
	models "autopilot-interview/models"
	cache "autopilot-interview/repositories/cache"
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockCacheProvider is a mock of CacheProvider interface.
type MockCacheProvider struct {
	ctrl     *gomock.Controller
	recorder *MockCacheProviderMockRecorder
}

// MockCacheProviderMockRecorder is the mock recorder for MockCacheProvider.
type MockCacheProviderMockRecorder struct {
	mock *MockCacheProvider
}

// NewMockCacheProvider creates a new mock instance.
func NewMockCacheProvider(ctrl *gomock.Controller) *MockCacheProvider {
	mock := &MockCacheProvider{ctrl: ctrl}
	mock.recorder = &MockCacheProviderMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockCacheProvider) EXPECT() *MockCacheProviderMockRecorder {
	return m.recorder
}

// Get mocks base method.
func (m *MockCacheProvider) Get(arg0 context.Context, arg1 string) (*cache.CacheResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", arg0, arg1)
	ret0, _ := ret[0].(*cache.CacheResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get indicates an expected call of Get.
func (mr *MockCacheProviderMockRecorder) Get(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockCacheProvider)(nil).Get), arg0, arg1)
}

// Invalidate mocks base method.
func (m *MockCacheProvider) Invalidate(arg0 context.Context) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Invalidate", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// Invalidate indicates an expected call of Invalidate.
func (mr *MockCacheProviderMockRecorder) Invalidate(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Invalidate", reflect.TypeOf((*MockCacheProvider)(nil).Invalidate), arg0)
}

// Status mocks base method.
func (m *MockCacheProvider) Status(arg0 context.Context) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Status", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// Status indicates an expected call of Status.
func (mr *MockCacheProviderMockRecorder) Status(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Status", reflect.TypeOf((*MockCacheProvider)(nil).Status), arg0)
}

// Update mocks base method.
func (m *MockCacheProvider) Update(arg0 context.Context, arg1 *models.Contact) (*cache.CacheResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", arg0, arg1)
	ret0, _ := ret[0].(*cache.CacheResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update.
func (mr *MockCacheProviderMockRecorder) Update(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockCacheProvider)(nil).Update), arg0, arg1)
}
