# autopilot-interview

this repository is a simple middleware implementation of the "contact/person" API of autopilot

## Content

---

- [Usage](#usage)
  - [Get contact from cache](#get-contact-from-cache)
  - [Create or Update contact](#create-or-update-contact)
  - [Status](#status)
  - [Invalidate](#invalidate)
- [Build](#build)
- [Environment Variables](#environment-variables)
- [Start](#start)
- [Test](#test)
- [Rebuild mock](#rebuild-mock)

## Usage

---

### Get contact from cache

```
GET /contacts/${id}
```

#### Response

```json
{
  "contact": {
    "id": "006231b92377d4aa50000000",
    "fields": {
      "bol::p": true,
      "str::email": "test@example.com",
      "str::first": "hello"
    }
  },
  "source": "cache",
  "ttl": 6378310700
}
```

#### Example

```shell
curl -XGET http://localhost:8080/contacts/006231b92377d4aa50000000
```

### Create or Update contact

```
POST /contacts/${id}
```

#### Body

```json
{
  "contact": {
    "fields": {
      "str::email": "pierre@example.com",
      "str::first": "pierre"
    }
  }
}
```

#### Response

```json
{
  "contact": {
    "id": "006231b92377d4aa50000000",
    "fields": {
      "str::email": "pierre@example.com",
      "str::first": "pierre"
    }
  }
}
```

#### Example

```shell
curl --request POST \
  --url http://localhost:8080/contacts \
  --header 'Content-Type: application/json' \
  --data '{
	"contact": {
		"fields": {
			"str::email": "pierre@example.com",
			"str::first": "pierre"
		}
	}
}'
```

### Status

Get the status of all subservices

```
GET /contacts/status
```

#### Response

```json
{
  "status": {
    "cache": true,
    "contact": true
  }
}
```

#### Example

```shell
curl -XGET http://localhost:8080/contacts/status
```

### Invalidate

flush the cache

```
DELETE /contacts/invalidate
```

#### Response

```json
{
  "invalidate": "ok"
}
```

#### Example

```shell
curl -XDELETE http://localhost:8080/contacts/invalidate
```

## Build

---

from scratch

```shell
$go build ./cmd/api
```

using docker

```shell
$ docker build -t autopilot-interview .
```

using docker-compose

```shell
$ docker-compose build
```

## Environment Variables

---

Value for the docker-compose are loaded from the .env file, you can use the provided [.env.example](.env.example)

| Key                    | Usage                                    | Example                    |
| ---------------------- | ---------------------------------------- | -------------------------- |
| API_PORT               | The Api port                             | 8080                       |
| API_AUTOPILOT_BASEPATH | Versionned basepath of the autopilot api | https://api.ap3api.com/v1/ |
| API_AUTOPILOT_KEY      | Private API key of autopilot             | xxxxx                      |
| API_REDIS_HOST         | The redis host                           | localhost:6379             |
| API_CACHE_DURATION     | value caching duration in nanosecond     | 10000000000                |
| GIN_MODE               | HTTP router mode                         | production                 |

## Start

---

from scratch, you needs to setup a redis yourself

```shell
$ ./api
```

using docker, you needs to setup a redis yourself

```shell
$ docker run autopilot-interview
```

using docker-compose, has a redis container

```shell
$ docker-compose up
```

## Test

---

```shell
$ go test ./...
```

## Rebuild mock

---

```shell
$ go generate ./...
```
