package main

import (
	"os"
	"strconv"
	"time"
)

const ENV_KEY_PREFIX = "API_"

type Config struct {
	Port string

	AutopilotKey      string
	AutopilotBasepath string

	RedisHost     string
	CacheDuration time.Duration
}

// Could add a panic on invalid value
func getConfig() *Config {
	duration, _ := strconv.ParseUint(os.Getenv(ENV_KEY_PREFIX+"CACHE_DURATION"), 10, 0)
	return &Config{
		Port: os.Getenv(ENV_KEY_PREFIX + "PORT"),

		AutopilotKey:      os.Getenv(ENV_KEY_PREFIX + "AUTOPILOT_KEY"),
		AutopilotBasepath: os.Getenv(ENV_KEY_PREFIX + "AUTOPILOT_BASEPATH"),
		RedisHost:         os.Getenv(ENV_KEY_PREFIX + "REDIS_HOST"),
		CacheDuration:     time.Duration(duration),
	}
}
