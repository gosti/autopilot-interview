package main

import (
	"autopilot-interview/adapters/autopilotapi"
	"autopilot-interview/adapters/redis"
	"autopilot-interview/service"
	"time"

	"github.com/gin-contrib/gzip"
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewProduction()

	config := getConfig()

	// Setup providers

	// can use in memory caching
	// cacheProvider := memory.New(config.CacheDuration)

	cacheProvider, err := redis.New(config.RedisHost, config.CacheDuration)
	if err != nil {
		panic(err)
	}

	contactProvider, err := autopilotapi.New(config.AutopilotBasepath, config.AutopilotKey)
	if err != nil {
		panic(err)
	}

	// Setup Service
	contactService := service.New(cacheProvider, contactProvider, logger)

	router := gin.New()

	router.Use(gzip.Gzip(gzip.DefaultCompression))
	router.Use(ginzap.Ginzap(logger, time.RFC3339, true))

	router.Use(ginzap.RecoveryWithZap(logger, true))

	// Add routes to router
	contactService.RegisterHTTPHandlers(router)

	router.Run(":" + config.Port)
}
