package redis

import (
	"autopilot-interview/models"
	"autopilot-interview/repositories/cache"
	"context"
	"errors"
	"time"

	"github.com/go-redis/redis/v8"
)

type Redis struct {
	client        *redis.Client
	cacheDuration time.Duration
}

func New(host string, cacheDuration time.Duration) (*Redis, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr: host,
	})

	redisProvider := &Redis{client: rdb, cacheDuration: cacheDuration}

	return redisProvider, redisProvider.Status(rdb.Context())
}

func (r *Redis) Get(ctx context.Context, ID string) (*cache.CacheResponse, error) {
	var contact models.Contact

	pipeline := r.client.Pipeline()
	ttl := pipeline.TTL(ctx, ID)
	value := pipeline.Get(ctx, ID)
	_, err := pipeline.Exec(ctx)
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return nil, cache.NewErrNotFound(ID, err)
		}

		return nil, err
	}

	value.Scan(&contact)

	err = r.client.Get(ctx, ID).Scan(&contact)

	if err != nil {
		if errors.Is(err, redis.Nil) {
			return nil, cache.NewErrNotFound(ID, err)
		}

		return nil, err
	}

	return &cache.CacheResponse{Contact: &contact, TTL: ttl.Val()}, nil
}

func (r *Redis) Update(ctx context.Context, contact *models.Contact) (*cache.CacheResponse, error) {
	err := r.client.SetEX(ctx, contact.ID, contact, r.cacheDuration).Err()
	if err != nil {
		return nil, err
	}

	return &cache.CacheResponse{Contact: contact, TTL: r.cacheDuration}, nil
}

func (r *Redis) Invalidate(ctx context.Context) error {
	return r.client.FlushAll(ctx).Err()
}

func (r *Redis) Status(ctx context.Context) error {
	return r.client.Ping(ctx).Err()
}
