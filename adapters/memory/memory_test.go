package memory

import (
	"autopilot-interview/models"
	"autopilot-interview/repositories/cache"
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestMemory_Get(t *testing.T) {
	m := New(-1)

	m.storage["abc"] = MemoryField{since: time.Now(), contact: models.Contact{ID: "abc", Fields: nil}}

	fmt.Println(m.Get(context.TODO(), "abc"))

	type args struct {
		ctx context.Context
		ID  string
	}
	tests := []struct {
		name        string
		args        args
		want        *cache.CacheResponse
		wantErr     bool
		wantErrToBe *cache.CacheError
	}{
		{name: "Get correct ID", args: args{ctx: context.Background(), ID: "abc"}, want: &cache.CacheResponse{Contact: &models.Contact{ID: "abc", Fields: nil}, TTL: -1}},
		{name: "Get incorrect ID", args: args{ctx: context.Background(), ID: "abcd"}, wantErr: true, wantErrToBe: &cache.ErrNotFound},
		{name: "Get incorrect ID", args: args{ctx: context.Background(), ID: "abcd"}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := m.Get(tt.args.ctx, tt.args.ID)
			if err != nil {
				if tt.wantErrToBe != nil && !tt.wantErrToBe.Is(err) {
					t.Errorf("Memory.Get() error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				if !tt.wantErr {
					t.Errorf("Memory.Get() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Memory.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
