package memory

import (
	"autopilot-interview/models"
	"autopilot-interview/repositories/cache"
	"context"
	"errors"
	"time"
)

type MemoryField struct {
	contact models.Contact // Could have been an interface but is directly the model for simplicity sake
	since   time.Time
}

type Memory struct {
	storage       map[string]MemoryField
	cacheDuration time.Duration
}

func New(cacheDuration time.Duration) *Memory {
	return &Memory{
		storage:       make(map[string]MemoryField),
		cacheDuration: cacheDuration,
	}
}

func (m *Memory) Get(ctx context.Context, ID string) (*cache.CacheResponse, error) {
	memoryField, ok := m.storage[ID]
	if !ok {
		return nil, cache.NewErrNotFound(ID, nil)
	}

	if m.cacheDuration == -1 {
		return &cache.CacheResponse{Contact: &memoryField.contact, TTL: -1}, nil
	}

	if memoryField.since.Add(m.cacheDuration).Before(time.Now()) {
		return nil, cache.NewErrExpired(ID, nil)
	}

	return &cache.CacheResponse{Contact: &memoryField.contact, TTL: m.cacheDuration - time.Since(memoryField.since)}, nil
}

func (m *Memory) Update(ctx context.Context, contact *models.Contact) (*cache.CacheResponse, error) {
	if contact == nil {
		return nil, errors.New("passing nil value")
	}
	m.storage[contact.ID] = MemoryField{
		contact: *contact,
		since:   time.Now(),
	}

	return &cache.CacheResponse{Contact: contact, TTL: m.cacheDuration}, nil
}

func (m *Memory) Invalidate(ctx context.Context) error {
	m.storage = map[string]MemoryField{}
	return nil
}

func (m *Memory) Status(ctx context.Context) error {
	return nil
}
