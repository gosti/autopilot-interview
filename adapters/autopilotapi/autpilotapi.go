package autopilotapi

import (
	"autopilot-interview/models"
	"autopilot-interview/repositories/contact"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Autopilotapi struct {
	basepath   string
	apikey     string
	httpClient http.Client
	baseUrl    *url.URL
}

const AUTOPILOT_AUTH_HEADER = "X-Api-Key"

func New(basepath string, apikey string) (*Autopilotapi, error) {
	if apikey == "" {
		return nil, errors.New("empty apikey")
	}

	baseUrl, err := url.Parse(basepath)
	if err != nil {
		return nil, err
	}

	return &Autopilotapi{basepath: basepath, apikey: apikey, httpClient: http.Client{Timeout: 5 * time.Second}, baseUrl: baseUrl}, nil
}

func (a *Autopilotapi) newAPIRequest(ctx context.Context, method, endpoint string, body io.Reader) (*http.Request, error) {
	parsedEndpoint, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}

	url := a.baseUrl.ResolveReference(parsedEndpoint)

	req, err := http.NewRequestWithContext(ctx, method, url.String(), body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	req.Header.Add(AUTOPILOT_AUTH_HEADER, a.apikey)
	return req, nil
}

func (a *Autopilotapi) Get(ctx context.Context, ID string) (*models.Contact, error) {
	var contactResponse AutoPilotPeopleGetByIDsResponse

	reqBody, err := json.Marshal(AutoPilotPeopleGetByIDsRequest{
		Fields:     []string{"str::first", "str::last", "str::email"}, // static for now, could pass it as a param ?
		ContactIDs: []string{ID},
	})
	if err != nil {
		return nil, err
	}

	req, err := a.newAPIRequest(ctx, "POST", "person/get-by-ids", bytes.NewReader(reqBody))
	if err != nil {
		return nil, err
	}
	resp, err := a.httpClient.Do(req)

	if err != nil {
		return nil, contact.NewErrUnexpected(ID, http.StatusInternalServerError, err)
	}

	if resp.StatusCode != http.StatusOK {
		b, _ := ioutil.ReadAll(resp.Body) // should import a autopilot error package

		switch resp.StatusCode {
		case http.StatusNotFound:
			return nil, contact.NewErrNotFound(ID, fmt.Errorf("%s", b))
		case http.StatusBadRequest:
			return nil, contact.NewErrBadRequest(ID, fmt.Errorf("%s", b))
		default:
			return nil, contact.NewErrUnexpected(ID, resp.StatusCode, fmt.Errorf("%s", b))
		}
	}

	err = json.NewDecoder(resp.Body).Decode(&contactResponse)
	if err != nil {
		return nil, err
	}

	if len(contactResponse.Contacts) != 1 {
		return nil, contact.NewErrNotFound(ID, nil)
	}

	for _, v := range contactResponse.Contacts {
		if strings.Compare(v.ID, ID) == 0 {
			return &v, nil
		}
	}

	return nil, nil
}

func (a *Autopilotapi) Update(ctx context.Context, updatedContact *models.Contact) (*contact.ConctactUpdateResponse, error) {
	var contactResponse AutoPilotPeopleUpdateResponse

	reqBody, err := json.Marshal(AutoPilotPeopleUpdateRequest{
		People: []*models.Contact{updatedContact},
	})
	if err != nil {
		return nil, err
	}

	req, err := a.newAPIRequest(ctx, "POST", "person/merge", bytes.NewReader(reqBody))
	if err != nil {
		return nil, err
	}
	resp, err := a.httpClient.Do(req)

	if err != nil {
		return nil, contact.NewErrUnexpected(updatedContact.ID, http.StatusInternalServerError, err)
	}

	if resp.StatusCode != http.StatusOK {
		b, _ := ioutil.ReadAll(resp.Body) // should import a autopilot error package
		return nil, contact.NewErrUnexpected("", resp.StatusCode, fmt.Errorf("%s", b))
	}

	err = json.NewDecoder(resp.Body).Decode(&contactResponse)
	if err != nil {
		return nil, err
	}

	if len(contactResponse.People) != 1 {
		return nil, fmt.Errorf("expect 1 people got %d", len(contactResponse.People))
	}

	updatedContact.ID = contactResponse.People[0].PersonID
	return &contact.ConctactUpdateResponse{Contact: updatedContact, Status: contactResponse.People[0].Status}, nil
}

func (a *Autopilotapi) Status(ctx context.Context) error {
	resp, err := a.httpClient.Get("https://api.ap3api.com/liveness-check") // hardcoded as not the same basepath as the api
	if err != nil {
		return err
	}

	value, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if strings.Compare(string(value), "ok") != 0 {
		return errors.New("bad response")
	}

	return nil
}
