package autopilotapi

import "autopilot-interview/models"

type AutoPilotPeopleUpdateRequest struct {
	People []*models.Contact `json:"people"`
}

type AutoPilotPeopleUpdateResponse struct {
	People []struct {
		PersonID string `json:"person_id"`
		Status   string `json:"status"`
	} `json:"people"`
}

type AutoPilotPeopleGetByIDsRequest struct {
	Fields     []string `json:"fields"`
	ContactIDs []string `json:"contact_ids"`
}

type AutoPilotPeopleGetByIDsResponse struct {
	Contacts map[string]models.Contact `json:"contacts"`
}
