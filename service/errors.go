package service

import (
	"fmt"
	"net/http"
)

type ServiceError struct {
	code    int
	message string
	err     error
}

func UnexpectedError(err error) *ServiceError {
	return &ServiceError{code: http.StatusInternalServerError, message: "an unexpected error happened", err: err}
}

func NotFoundError(err error) *ServiceError {
	return &ServiceError{code: http.StatusNotFound, message: "not found", err: err}
}

func BadRequestError(err error) *ServiceError {
	return &ServiceError{code: http.StatusBadRequest, message: "bad request", err: err}
}

func (s *ServiceError) Error() string {
	if s.err != nil {
		return fmt.Sprintf("%d: %s err: %s", s.code, s.message, s.err)
	}
	return fmt.Sprintf("%d: %s", s.code, s.message)
}
