package service

import (
	"autopilot-interview/mocks"
	"autopilot-interview/models"
	"autopilot-interview/repositories/cache"
	"autopilot-interview/repositories/contact"
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest"
)

type mockedService struct {
	Cache   *mocks.MockCacheProvider
	Contact *mocks.MockContactProvider
}

func TestService_Get(t *testing.T) {

	type params struct {
		ctx context.Context
		ID  string
	}

	for n, c := range map[string]struct {
		params         params
		configure      func(*mockedService)
		checkResponse  func(*testing.T, *ContactResponse, error)
		loggerOverride *zap.Logger
	}{
		"find user in cache": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ms.Cache.EXPECT().Get(context.TODO(), "abc").Return(&cache.CacheResponse{Contact: &models.Contact{ID: "abc", Fields: nil}, TTL: time.Second}, nil)
			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				ttl := time.Second
				require.Equal(t, cr, &ContactResponse{Source: "cache", Contact: &models.Contact{ID: "abc", Fields: nil}, TTL: &ttl}, "the response is valid")
			},
		},
		"user not in cache": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ttl := time.Second

				ms.Cache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, cache.NewErrNotFound("abc", nil))
				ms.Contact.EXPECT().Get(context.TODO(), "abc").Return(&models.Contact{ID: "abc", Fields: nil}, nil)
				ms.Cache.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: nil}).Return(&cache.CacheResponse{Contact: &models.Contact{ID: "abc", Fields: nil}, TTL: ttl}, nil)
			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				require.Equal(t, cr, &ContactResponse{Source: "api", Contact: &models.Contact{ID: "abc", Fields: nil}}, "the response is valid")
			},
		},
		"cache throw unknown error": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ms.Cache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, errors.New("random error"))
			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				require.Error(t, err, "the cache throwed an unexpected error")
			},
		},
		"api sent not found": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ms.Cache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, cache.NewErrNotFound("abc", nil))
				ms.Contact.EXPECT().Get(context.TODO(), "abc").Return(nil, contact.NewErrNotFound("abc", nil))

			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				require.Equal(t, err.(*ServiceError).code, http.StatusNotFound, "have the correct error code")
			},
		},
		"api sent bad request": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ms.Cache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, cache.NewErrNotFound("abc", nil))
				ms.Contact.EXPECT().Get(context.TODO(), "abc").Return(nil, contact.NewErrBadRequest("abc", nil))

			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				require.Equal(t, err.(*ServiceError).code, http.StatusBadRequest, "have the correct error code")
			},
		},
		"cache fails to write": {
			params: params{ctx: context.TODO(), ID: "abc"},
			configure: func(ms *mockedService) {
				ms.Cache.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, cache.NewErrNotFound("abc", nil))
				ms.Contact.EXPECT().Get(context.TODO(), "abc").Return(&models.Contact{ID: "abc", Fields: nil}, nil)
				ms.Cache.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: nil}).Return(nil, errors.New("fails"))
			},
			checkResponse: func(t *testing.T, cr *ContactResponse, err error) {
				require.Equal(t, cr, &ContactResponse{Source: "api", Contact: &models.Contact{ID: "abc", Fields: nil}}, "the response is valid")
			},
			loggerOverride: zaptest.NewLogger(t, zaptest.WrapOptions(zap.Hooks(func(e zapcore.Entry) error {
				if e.Level != zap.ErrorLevel {
					t.Fatal("error should have happened")
				}
				return nil
			}))),
		},
	} {
		t.Run(n, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			logger := zaptest.NewLogger(t)
			if c.loggerOverride != nil {
				logger = c.loggerOverride
			}
			mockCache := mocks.NewMockCacheProvider(mockCtrl)
			mockContact := mocks.NewMockContactProvider(mockCtrl)
			service := New(mockCache, mockContact, logger)
			mck := &mockedService{Cache: mockCache, Contact: mockContact}

			if c.configure != nil {
				c.configure(mck)
			}

			resp, err := service.Get(c.params.ctx, c.params.ID)
			// Check response
			if c.checkResponse != nil {
				c.checkResponse(t, resp, err)
			}
		})
	}
}

func TestService_Update(t *testing.T) {

	type params struct {
		ctx            context.Context
		updatedContact *UpdateContactRequest
	}

	for n, c := range map[string]struct {
		params         params
		configure      func(*mockedService)
		checkResponse  func(*testing.T, *UpdateContactResponse, error)
		loggerOverride *zap.Logger
	}{
		"insert new user": {
			params: params{ctx: context.TODO(), updatedContact: &UpdateContactRequest{Contact: &models.Contact{Fields: nil}}},
			configure: func(ms *mockedService) {
				ms.Contact.EXPECT().Update(context.TODO(), &models.Contact{ID: "", Fields: nil}).
					Return(&contact.ConctactUpdateResponse{Status: "created", Contact: &models.Contact{ID: "abc", Fields: nil}}, nil)

				ms.Cache.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: nil}).
					Return(&cache.CacheResponse{Contact: &models.Contact{ID: "abc", Fields: nil}, TTL: time.Second}, nil)

			},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Equal(t, cr, &UpdateContactResponse{Status: "created", Contact: &models.Contact{ID: "abc", Fields: nil}}, "the response is valid")
			},
		},
		"update user": {
			params: params{ctx: context.TODO(), updatedContact: &UpdateContactRequest{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}}},
			configure: func(ms *mockedService) {
				ms.Contact.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}).
					Return(&contact.ConctactUpdateResponse{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}, Status: "merged"}, nil)

				ms.Cache.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}).
					Return(&cache.CacheResponse{TTL: time.Second, Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}}, nil)

			},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Equal(t, cr, &UpdateContactResponse{Status: "merged", Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}}, "the response is valid")
			},
		},
		"api fails": {
			params: params{ctx: context.TODO(), updatedContact: &UpdateContactRequest{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}}},
			configure: func(ms *mockedService) {
				ms.Contact.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}).
					Return(nil, errors.New("random error"))
			},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Error(t, err, "the response is valid")
			},
		},
		"caching fails": {
			params: params{ctx: context.TODO(), updatedContact: &UpdateContactRequest{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}}},
			configure: func(ms *mockedService) {
				ms.Contact.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}).
					Return(&contact.ConctactUpdateResponse{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}, Status: "merged"}, nil)

				ms.Cache.EXPECT().Update(context.TODO(), &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}).Return(nil, errors.New("random error"))

			},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Equal(t, cr, &UpdateContactResponse{Contact: &models.Contact{ID: "abc", Fields: models.Fields{"str::name": "Pierre"}}, Status: "merged"}, "the response is valid")
			},
			loggerOverride: zaptest.NewLogger(t, zaptest.WrapOptions(zap.Hooks(func(e zapcore.Entry) error {
				if e.Level != zap.ErrorLevel {
					t.Fatal("error should have happened")
				}
				return nil
			}))),
		},
		"passing nil value": {
			params:    params{ctx: context.TODO(), updatedContact: nil},
			configure: func(ms *mockedService) {},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Error(t, err, "the response is valid")
			},
		},
		"passing nil contact": {
			params: params{ctx: context.TODO(), updatedContact: &UpdateContactRequest{Contact: nil}},
			configure: func(ms *mockedService) {
				ms.Contact.EXPECT().Update(context.TODO(), nil).
					Return(nil, contact.ErrBadRequest)
			},
			checkResponse: func(t *testing.T, cr *UpdateContactResponse, err error) {
				require.Error(t, err, "the response is valid")
			},
		},
	} {
		t.Run(n, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			logger := zaptest.NewLogger(t)
			if c.loggerOverride != nil {
				logger = c.loggerOverride
			}
			mockCache := mocks.NewMockCacheProvider(mockCtrl)
			mockContact := mocks.NewMockContactProvider(mockCtrl)
			service := New(mockCache, mockContact, logger)
			mck := &mockedService{Cache: mockCache, Contact: mockContact}

			if c.configure != nil {
				c.configure(mck)
			}

			resp, err := service.Update(c.params.ctx, c.params.updatedContact)
			// Check response
			if c.checkResponse != nil {
				c.checkResponse(t, resp, err)
			}
		})
	}
}
