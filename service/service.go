package service

import (
	"autopilot-interview/repositories/cache"
	"autopilot-interview/repositories/contact"

	"go.uber.org/zap"
)

type Service struct {
	cache   cache.CacheProvider
	contact contact.ContactProvider
	logger  *zap.Logger
}

func New(cache cache.CacheProvider, contact contact.ContactProvider, logger *zap.Logger) *Service {
	return &Service{cache: cache, contact: contact, logger: logger}
}
