package service

import (
	"autopilot-interview/models"
	"time"
)

const (
	SourceCache = "cache"
	SourceAPI   = "api"
)

type ContactResponse struct {
	Contact *models.Contact `json:"contact"`
	Source  string          `json:"source"` // just to help debug and add context to the interview
	TTL     *time.Duration  `json:"ttl,omitempty"`
}

type UpdateContactRequest struct {
	Contact *models.Contact `json:"contact" binding:"required"`
}

type UpdateContactResponse struct {
	Contact *models.Contact `json:"contact"`

	Status string `json:"status"`
}

func marshalContactResponse(contact *models.Contact, source string, ttl *time.Duration) *ContactResponse {
	return &ContactResponse{
		Contact: contact,
		Source:  source,
		TTL:     ttl,
	}
}

func marshalUpdateContactResponse(contact *models.Contact, status string) *UpdateContactResponse {
	return &UpdateContactResponse{
		Contact: contact,

		Status: status,
	}
}

type StatusResponse struct {
	Status struct {
		Cache   bool `json:"cache"`
		Contact bool `json:"contact"`
	} `json:"status"`
}
