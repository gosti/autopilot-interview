package service

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (s *Service) RegisterHTTPHandlers(router gin.IRouter) {
	group := router.Group("contacts")

	group.GET("/status", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, s.Status(ctx))
	})

	group.DELETE("/invalidate", func(ctx *gin.Context) {
		err := s.Invalidate(ctx)
		if err != nil {
			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"invalidate": "ok"})
	})

	group.GET(":id", func(ctx *gin.Context) {
		contact, err := s.Get(ctx, ctx.Param("id"))

		if err != nil {
			if serviceErr, ok := err.(*ServiceError); ok {
				ctx.AbortWithError(serviceErr.code, err)
				return
			}

			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		ctx.JSON(http.StatusOK, contact)
	})

	group.POST("", func(ctx *gin.Context) {
		var contact UpdateContactRequest

		err := ctx.BindJSON(&contact)
		if err != nil {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}

		resp, err := s.Update(ctx, &contact)
		if err != nil {
			if serviceErr, ok := err.(*ServiceError); ok {
				ctx.AbortWithError(serviceErr.code, err)
				return
			}

			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		ctx.JSON(http.StatusOK, resp)
	})

	s.logger.Info("Service routes registered")
}
