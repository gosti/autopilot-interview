package service

import (
	"autopilot-interview/repositories/cache"
	"autopilot-interview/repositories/contact"
	"context"
	"net/http"

	"go.uber.org/zap"
)

func (s *Service) Get(ctx context.Context, ID string) (*ContactResponse, error) {
	fromCache, err := s.cache.Get(ctx, ID)

	if err == nil {
		return marshalContactResponse(fromCache.Contact, SourceCache, &fromCache.TTL), nil
	}

	// skip "non critical" error
	if !cache.ErrNotFound.Is(err) && !cache.ErrExpired.Is(err) {
		return nil, UnexpectedError(err)
	}

	fromAPI, err := s.contact.Get(ctx, ID)
	if err != nil {
		if contactErr, ok := err.(*contact.ContactError); ok && contactErr.Code == http.StatusNotFound {
			return nil, NotFoundError(err)
		}

		return nil, BadRequestError(err)
	}

	_, err = s.cache.Update(ctx, fromAPI)
	if err != nil {
		s.logger.Error("Caching of fresh value failed", zap.Error(err))
	}

	return marshalContactResponse(fromAPI, SourceAPI, nil), nil
}

func (s *Service) Update(ctx context.Context, updatedContact *UpdateContactRequest) (*UpdateContactResponse, error) {
	if updatedContact == nil {
		return nil, BadRequestError(nil)
	}

	fromAPI, err := s.contact.Update(ctx, updatedContact.Contact)
	if err != nil {
		return nil, BadRequestError(err)
	}

	if _, err := s.cache.Update(ctx, fromAPI.Contact); err != nil {
		s.logger.Error("Caching of fresh value failed, returning api response", zap.Error(err))
	}

	return marshalUpdateContactResponse(fromAPI.Contact, fromAPI.Status), nil
}

func (s *Service) Invalidate(ctx context.Context) error {
	return s.cache.Invalidate(ctx)
}

func (s *Service) Status(ctx context.Context) *StatusResponse {
	var status StatusResponse

	if err := s.cache.Status(ctx); err != nil {
		s.logger.Error("cache fails with:", zap.Error(err))
	} else {
		status.Status.Cache = true
	}
	if err := s.contact.Status(ctx); err != nil {
		s.logger.Error("contact fails with:", zap.Error(err))
	} else {
		status.Status.Contact = true
	}

	return &status
}
